#ifndef _hash_h
#define _hash_h

#include "GList.h"

typedef unsigned (*FuncionHash)(void* clave);
typedef int (*FuncionIgualdad)(void *, void *);


typedef struct {
    GList* tabla;
    int numElems;
    int capacidad;
    FuncionHash hash;
    FuncionIgualdad igualdad;
    Destruir destruir;

} TablaHash;

/**
tablahash_crear: int, FuncionHash, Igualdad, Destruir -> TablaHash*
Recibe como parametro la capacidad de la tabla, la funcion de hasheo, una funcion igualdad
que tiene como objetivo decir si 2 palabras son iguales y una funcion destruir que se encarga
de liberar la memoria una palabra dentro de la estructura word y de la estructura en si.
Crea una estructura tabla hash, asigna las funciones, e inicializa cada puntero de nuestra
tabla como NULL. Finalmente, devuelve un puntero a la estructura creada.
*/

TablaHash* tablahash_crear(int capacidad, FuncionHash fun, Igualdad ig, Destruir borrardato);

/**
tablashash_insertar: TablaHash*, void*
Recibe como parametro un puntero a estructura tabla hash y un dato (como void*).
Calcula la clave para el dato, si el puntero de la posicion correspondiente a la clave es NULL,
crea una GList e inserta el dato. En caso contrario, inserta el dato al final de la lista.
*/
void tablahash_insertar(TablaHash* tabla, void* dato);

/**
tablahash_buscar: TablaHash*, void* -> int
Recibe como parametro un puntero a estructura tabla hash y un dato (como void*).
Calcula la clave para ese dato, busca el elemento dado en la tabla, y retorna
un puntero al mismo. En caso de no existir, se retorna un puntero nulo.
 */
int tablahash_buscar(TablaHash* tabla, void* dato);

/**
tablahash_destruir: TablaHash*
Recibe como parametro un puntero a estructura tabla hash, libera la memoria de cada lista
de la tabla, de la tabla y de la estructura.
*/
void tablahash_destruir(TablaHash* tabla);

void tablahash_diagnostico(TablaHash* tabla, Imprimir imp);


#endif
