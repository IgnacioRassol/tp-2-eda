#include <stdio.h>
#include <wchar.h>
#include <locale.h>
#include <assert.h>
#include <malloc.h>

#include "GList.h"

typedef struct palabra_input{
    wchar_t* pal;
    int linea;
} *Palabra;             //Lo hice puntero pq la funcion de agregar toma puntero y queda feo con el &

////////////////////// FUNCIONES GLIST ////////////////////////

int GList_existe(GList inicio) {
    return (inicio != NULL);
}

int GList_vacio(GList inicio) {
    return(inicio->cantidad_elementos == 0);
}

GList GList_crear() {
    return GList_crear_header();
}

GList GList_crear_header() {
    GList nuevoHeader;
    nuevoHeader = malloc(sizeof(GHeader));
    nuevoHeader->principio = NULL;
    nuevoHeader->fin = NULL;
    nuevoHeader->cantidad_elementos = 0;

    return nuevoHeader;
}

GList GList_agregar_final (GList inicio, void* dato) {
    GNodoP nuevoNodo;

    nuevoNodo = malloc(sizeof(GNodo));

    nuevoNodo->dato = dato;
    nuevoNodo->sig = NULL;

    if (GList_vacio(inicio)) {
        inicio->principio = nuevoNodo;
        inicio->fin = nuevoNodo;
    } else{
        GNodoP ultimoNodo = inicio->fin;
        ultimoNodo->sig = nuevoNodo;
        inicio->fin = nuevoNodo;
    }

    inicio->cantidad_elementos++;
    return inicio;

}

void GList_imprimirxD (GList inicio) {
    GNodoP nodo;

    nodo = inicio->principio;
    for (; nodo;) {
        wprintf(L"Palabra: %ls - Linea: %d\n", ((Palabra)(nodo->dato))->pal, ((Palabra)(nodo->dato))->linea);
        nodo = nodo->sig;
    }
}

///////////////////////////////////////////////////////////////


/////////////////// METODOS FINALES ///////////////////////////

void separar_palabra (wchar_t palabra[], int largoPalabra, int posicion,
                      wchar_t* palabra1, wchar_t* palabra2){

    int largo1, largo2, contador = 0;
    largo1 = posicion;
    largo2 = largoPalabra - posicion;

    wcsncpy(palabra1, palabra, largo1);
    palabra1[largo1] = 0;
    for (int i = largo1; i < largoPalabra; i++){
        palabra2[contador] = palabra[i];
        contador++;
    }
    palabra2[largo2] = 0;
}

void intercambio (wchar_t palabra[], int largoPalabra, int posicion, wchar_t* output){

    if (posicion > 0){
        wcscpy(output, palabra);
        output[posicion] = palabra[posicion-1];
        output[posicion-1] = palabra[posicion];
    }
}

void eliminacion(wchar_t palabra[], int largoPalabra, int indice, wchar_t* nuevaPalabra){
    int contadorPalabra, indiceNuevaPalabra = 0;

    for (contadorPalabra = 0; palabra[contadorPalabra]; contadorPalabra++){
        if (contadorPalabra != indice){
            nuevaPalabra[indiceNuevaPalabra] = palabra[contadorPalabra];
            indiceNuevaPalabra++;
        }
    }

    nuevaPalabra[indiceNuevaPalabra] = '\0';
}

void agregar_entre_letras (wchar_t palabra[], wchar_t* nuevaPalabra, int largoNuevaPalabra, int indice, wchar_t letra){  //anda

    int contador = 0;

    for (int i = 0; i < largoNuevaPalabra; i++){  //Copia las letras excepto en el indice a cambiar
        if (i != indice){
            nuevaPalabra[i] = palabra[contador];
            contador++;
        }
    }

    nuevaPalabra[indice] = letra;
    nuevaPalabra[largoNuevaPalabra] = 0;

}

void reemplazo (wchar_t palabra[], int largoPalabra, wchar_t* nuevaPalabra, int indice, wchar_t letra){  //anda
    wcscpy(nuevaPalabra, palabra);
    nuevaPalabra[indice] = letra;
}

////////////////////////////////////////////////////////////////////////

wchar_t wtoupper (wchar_t letra){
    if (letra >= 97 && letra <= 122 || letra >= 225){
        return (letra - 32);
    } else {
        return letra;
    }
}

wchar_t wtolower (wchar_t letra){
    if (letra >= 65 && letra <= 90 || letra <= 220 && letra >= 193){
        return (letra + 32);
    } else {
        return letra;
    }
}

wchar_t* a_minuscula (wchar_t palabra[]) {
    int largoPalabra = wcslen(palabra);
    wchar_t* nuevaPalabra;
    nuevaPalabra = malloc(sizeof(wchar_t)*largoPalabra);
    for (int i = 0; i < largoPalabra; i++){
        nuevaPalabra[i] = wtolower(palabra[i]);
    }
    return nuevaPalabra;
}
/*
wchar_t* eliminacionOP(wchar_t palabra[], int indice){
    int largoPalabra = wcslen(palabra), contadorPalabra, indiceNuevaPalabra = 0;
    wchar_t* nuevaPalabra = malloc(sizeof(wchar_t)*largoPalabra);
    for (contadorPalabra = 0; contadorPalabra < largoPalabra; contadorPalabra++){
        if (contadorPalabra != indice){
            nuevaPalabra[indiceNuevaPalabra] = palabra[contadorPalabra];
            indiceNuevaPalabra++;
        }
    }

    nuevaPalabra[contadorPalabra] = '\0';
    wprintf(L"Elimina3: %ls\n", nuevaPalabra);
}

void separar_palabra (wchar_t palabra[]){
    int largoPalabra, cantidadSeparadas, filaMatriz = 0, contador, posPalabra;
    largoPalabra = wcslen(palabra);
    cantidadSeparadas = (largoPalabra - 1) * 2;
    wchar_t separadas[cantidadSeparadas][15];
    for (int posEspacio = 1; posEspacio < largoPalabra; posEspacio++){
        contador = 0;
        for (posPalabra = 0; posPalabra < largoPalabra; posPalabra++){
            if (posPalabra == posEspacio){
                separadas[filaMatriz][contador] = '\0';
                filaMatriz++;
                contador = 0;
                separadas[filaMatriz][contador] = palabra[posPalabra];
                contador++;
            } else {
                separadas[filaMatriz][contador] = palabra[posPalabra];
                contador++;
            }
        }
        separadas[filaMatriz][contador] = '\0';
        filaMatriz++;
    }
    for (int i = 0; i < cantidadSeparadas; i++){
        wprintf(L"%ls\n", separadas[i]);
    }
}


void intercambio (wchar_t palabra[]){
    int largoPalabra;
    wchar_t aux;
    largoPalabra = wcslen(palabra);
    for (int i = 1; i < largoPalabra; i++){
        aux = palabra[i];
        palabra[i] = palabra[i-1];
        palabra[i-1] = aux;
        wprintf(L"%ls\n", palabra);
        palabra[i-1] = palabra[i];
        palabra[i] = aux;
    }
}


void agregar_entre_letras (wchar_t palabra[], wchar_t alfabeto[]){  //anda
    int largoNuevaPalabra, indiceACambiar = 0, contador;
    largoNuevaPalabra = wcslen(palabra) + 1;
    wchar_t nuevaPalabra[largoNuevaPalabra+1]; //para el terminador
    nuevaPalabra[largoNuevaPalabra] = '\0';
    while (indiceACambiar < largoNuevaPalabra){
        contador = 0;
        for (int i = 0; i < largoNuevaPalabra; i++){  //Copia las letras excepto en el indice a cambiar
            if (i != indiceACambiar){
                nuevaPalabra[i] = palabra[contador];
                contador++;
            }
        }
        for (int i = 0; alfabeto[i]; i++){
            nuevaPalabra[indiceACambiar] = alfabeto[i];
            wprintf(L"%ls\n", nuevaPalabra);
        }
        indiceACambiar++;
    }
}


void reemplazo (wchar_t palabra[], wchar_t alfabeto[]){
    int largoPalabra;
    FILE* arxivo;
    arxivo = fopen("prueba2.txt","w");
    largoPalabra = wcslen(palabra);
    wchar_t aux; //para no perder la letra que reemplazo
    for (int i = 0; i < largoPalabra; i++){
        aux = palabra[i];
        for (int j = 0; alfabeto[j]; j++){
            palabra[i] = alfabeto[j];
            fwprintf (arxivo, L"%ls\n",palabra);
        }
        palabra[i] = aux;
    }
    fclose(arxivo);
}


void eliminacion (wchar_t palabra[]){              //funciona
    int largoPalabra , contadorPalabra;
    largoPalabra = wcslen(palabra);
    wprintf(L"Largo palabra: %d\n", largoPalabra);
    wchar_t aux[largoPalabra];
    for (int indiceMalo = 0; indiceMalo < largoPalabra; indiceMalo++){
        contadorPalabra = 0;
        for (int indiceNuevaPalabra = 0; indiceNuevaPalabra < largoPalabra;) {
            if (contadorPalabra == indiceMalo){
                contadorPalabra++;
            }else{
                aux[indiceNuevaPalabra] = palabra[contadorPalabra];
                indiceNuevaPalabra++;
                contadorPalabra++;
            }
        }

        wprintf(L"%ls\n", aux);
    }
}
*/
int calcular_cantidad(char nombreArchivo[]){  //cuenta bien
  int contador = 0;
  FILE* archivo;
  char aux[25];
  archivo = fopen(nombreArchivo, "r");
  while (fscanf(archivo,"%s", aux) == 1){  //por algun motivo el !feof no me anda mas asique lo hice asi xd
    contador++;
  }

  fclose(archivo);
  return contador;

}

int archivo_valido(char nombreArchivo[]){
  FILE* archivo;
  char aux;
  archivo = fopen(nombreArchivo, "r");
  if (archivo != NULL){
    aux = fgetc (archivo);
    fclose(archivo);
    if (aux != EOF) {
        return 1;       // Existe y no vacio
    }
  }
  return 0;             // Esta vacio o no existe
}

void insertarPalabra (wchar_t palabras[][40], wchar_t aux[], int i){
    wcscpy(palabras[i], aux);
}

void insertar (char nombreArchivo[], wchar_t palabras[][40], int cantidadPalabras){
    FILE* archivo;
    archivo = fopen(nombreArchivo, "r");
    wchar_t aux[40];
    for (int i = 0; i < cantidadPalabras; i++){
        printf("Se va a insertar la palabra: %d\n", i);
        fwscanf(archivo, L"%ls", aux);
        insertarPalabra(palabras, aux, i);
    }
}

void imprimir_matriz (wchar_t matriz[][10], int filas){
  for (int i = 0; i < filas; i++){
    wprintf(L"Fila %d: %ls", i, matriz[i]);
  }
}

GList insertar_palabra(wchar_t* aux, GList inicio){
    int largoPalabra = wcslen(aux);
    wchar_t* nuevoNodo;
    if (aux[largoPalabra-1] < 64 || aux[largoPalabra-1] == '\n'){
        aux[largoPalabra-1] = '\0';                                  //el wcscpy copia el terminador
        nuevoNodo = malloc(sizeof(wchar_t) * largoPalabra);
        wcscpy(nuevoNodo, aux);
        inicio = GList_agregar_final(inicio, nuevoNodo);
    } else {
        aux [largoPalabra] = '\0';
        nuevoNodo = malloc(sizeof(wchar_t) * (largoPalabra + 1));
        wcscpy(nuevoNodo, aux);
        inicio = GList_agregar_final(inicio, nuevoNodo);
    }

    return inicio;
}

GList agregar_palabra (GList inicio, wchar_t buffer[], int contadorLinea) {
    Palabra newPalabra;
    newPalabra = malloc(sizeof(struct palabra_input));
    int largoPalabra = wcslen(buffer);

    newPalabra->pal = malloc(sizeof(wchar_t) * (largoPalabra+1));
    wcscpy(newPalabra->pal, buffer);
    newPalabra->linea = contadorLinea;
    inicio = GList_agregar_final(inicio, newPalabra);

    return inicio;
}

GList parsear_palabras (wchar_t aux[], int contadorLinea, GList inicio) {
    if (aux[0] != '\n'){
        wchar_t buffer[20];
        int contadorBuffer = 0, largoLinea = wcslen(aux);
        for (int i = 0; i < largoLinea; i++){
            if (aux[i] != ' ' && aux[i] != '\n' && aux[i] != '\r'){
                buffer[contadorBuffer] = aux[i];
                contadorBuffer++;
            } else {
                if (aux[i-1] <= 64){
                    buffer[contadorBuffer - 1] = '\0';
                } else {
                    buffer[contadorBuffer] = '\0';
                }
                contadorBuffer = 0;
                inicio = agregar_palabra (inicio, buffer, contadorLinea);
                //Aca deberia llamarse a la que checkea si la letra esta en el universo y da las sugerencias sino
                // (en vez de agregar palabra me refiero)
            }

        }
    }

    return inicio;
}


GList lectura_entrada (char ruta[], GList inicio) {
    FILE* archivo;
    int contadorLinea = 1;
    archivo = fopen(ruta,"r");
    assert(archivo != NULL);
    wchar_t aux[100];
    while (fgetws (aux , 100 , archivo) != NULL ){
        inicio = parsear_palabras(aux, contadorLinea, inicio);
        contadorLinea++;
    }

    fclose(archivo);
    return inicio;
}

void output (wchar_t* palabra, int linea, GList sugerencias, FILE* archivoSalida){
    fwprintf (archivoSalida, L"Linea %d, %ls no esta en el diccionario.\nQuizas quiso decir: ", linea, palabra);

    GNodoP nodo;
    nodo = inicio->principio;

    for (; nodo;) {
        fwprintf(archivoSalida,L"%ls ", ((Palabra)(nodo->dato))->pal);
        nodo = nodo->sig;
    }

    fwprintf (archivoSalida, L"\n");

}



int main(){
    fwide(stdout, 1);
    //char nombreArchivo[25];
    //int cantidadPalabras;
    setlocale(LC_CTYPE,"");
    /*wchar_t alfabeto[70];

    wchar_t palabra[20], palabra2[20];
    FILE* archivo;
    archivo = fopen("palabra.txt","r");
    fwscanf (archivo, L"%ls", palabra);

    fclose(archivo);

    reemplazo (wchar_t palabra[], int largoPalabra, wchar_t* nuevaPalabra,
    int indice, wchar_t letra)
void separar_palabra (wchar_t palabra[], int largoPalabra, int posicion,
                      wchar_t* palabra1, wchar_t* palabra2)
*/
    GList inicio;
    inicio = GList_crear();
    char ruta[20] = "prueba.txt";
    wchar_t pal[15], palaux[15], palaux2[15];

    inicio = lectura_entrada(ruta, inicio);
    GList_imprimirxD(inicio);




    while (archivo_valido(nombreArchivo) != 1){
        printf("El archivo no existe o no se puede abrir.\nIngrese nuevamente el nombre del archivo: ");
        scanf("%s", nombreArchivo);
    }
    cantidadPalabras = calcular_cantidad(nombreArchivo);
    printf("Cantidad de palabras: %d\n", cantidadPalabras);
    system("pause");
    wchar_t palabras[cantidadPalabras][40];

    insertar(nombreArchivo, palabras, cantidadPalabras);  //Nose si queres pasarle la tabla hash de parametro o que la haga adentro
    FILE* archivoPrueba;
    archivoPrueba = fopen("prueba.txt","w");
    for (int i = 0; i < cantidadPalabras; i++){
        printf("Palabra: %d\n", i);
        fwprintf(archivoPrueba, L"Palabra %d: %ls\n", i, palabras[i]);
    }
    fclose(archivoPrueba);
*/
    return 0;
}

/*
setlocale(LC_CTYPE,"");
    FILE* archivo;
    archivo = fopen("caracteresRaros.txt","r");
    wchar_t palabraw[10][20];
    while(fwscanf(archivo, L"%ls", palabraw[cont]) == 1){
        wprintf(L"Renglon %d: %ls\n", cont, palabraw[cont]);
        for (int i = 0; palabraw[cont][i]; i++){
            wprintf(L"Caracter %d: %lc\n", i, palabraw[cont][i]);
        }
        cont++;
    }
    fclose(archivo);
  */
