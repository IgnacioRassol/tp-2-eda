#ifndef _GList_h
#define _GList_h

#include <stdio.h>


/*
Una GList es una lista generica, compuesta por un header y una cantidad arbitraria de nodos.
Una lista vacia no presenta ninguno de estos elementos, no puede existir una lista con el header
y ningun nodo, o viceversa, una lista de nodos sin el header.
El header consiste en un puntero al primer y ultimo nodo, para facilitar agregar elementos
al final de la lista.
Los nodos consisten en un puntero al siguiente nodo, y un puntero al tipo de dato que almacenan,
el cual puede ser cualquiera.
*/

//Definicion de un nodo

typedef struct _GNodo {
    void* dato;
    struct _GNodo* sig;
} GNodo;

//Definicion del header

typedef struct {
    GNodo* principio;
    GNodo* fin;
    int cantidad_elementos;
} GHeader;

// Definiciones de punteros a elementos de la estructura

typedef GHeader* GList;
typedef GNodo* GNodoP;

// Definiciones de punteros a funciones.

typedef void  (*Destruir)  (void*);
typedef void* (*Copia)     (void*);
typedef void* (*Funcion)   (void*);
typedef int   (*Predicado) (void*);
typedef void  (*Imprimir)  (void*, FILE*);
typedef int   (*Igualdad)  (void *, void *);



/**
GList_vacio: GList -> Int
Recibe como parametro una GList. Si esta vacio, devuelve algo distinto
de 0.
*/
int GList_vacio(GList);

/**
GList_crear: -> GList
Retorna NULL
*/
GList GList_crear();

/**
GList_crear_header: -> GList
Crea un puntero a la estructura GHeader y le asigna NULL a ambos punteros de la estructura.
*/
GList GList_crear_header();

/**
GList_agregar_inicio: GList, Void* -> GList
Recibe como parametro una GList y un dato (como void*), crea un nuevo nodo y guarda el dato recibido como parametro.
Si nuestra lista esta vacia, crea la lista e inserta nuestro nodo en la lista. En caso contrario, simplemente inserta nuestro
nuevo nodo en la primer posicion de la lista.
*/
GList GList_agregar_inicio(GList, void*);

/**
GList_agregar_final: GList, Void* -> GList
Recibe como parametro una GList y un dato (como void*), crea un nuevo nodo y guarda el dato recibido como parametro.
Si nuestra lista esta vacia, crea la lista e inserta nuestro nodo en la lista. En caso contrario, simplemente inserta nuestro
nuevo nodo en la ultima posicion de la lista.
*/
GList GList_agregar_final(GList, void*);

/**
GList_destruir: GList, Destruir
Recibe como parametro una GList y un puntero a una funcion que se encarga de eliminar los datos. Recorre toda la lista
y se encarga de liberar la memoria de todos los nodos de la misma, finalmente liberando el header.
*/
void GList_destruir(GList, Destruir);

/**
GList_destruir: GList, Char*, Imprimir
Recibe como parametro una GList, el nombre del archivo de salida y un puntero a una funcion que se encarga de imprimir
los datos en el archivo de salida. Para esto, recorre la lista y le aplica dicha funcion a los datos de los nodos
*/
void GList_imprimir(GList, char*, Imprimir);

/**
map: GList, Funcion, Copia -> GList
Map tiene como objetivo aplicarle a todos los nodos de una lista, un funcion. Para esto recibe como parametro una GList y dos punteros a funcion,
donde la segunda funcion va a ser la encargada de copiar los nodos de la GList original y la primera funcion sera la que aplicaremos
a cada nodo. Finalmente, devuelve una nueva GList compuesta por los nodos copiados luego de haberles aplicado la primer funcion.
*/
GList map (GList, Funcion, Copia);

/**
filter: GList, Predicado, Copia -> GList
Filter tiene como objetivo filtrar los nodos de una lista mediante un predicado. Para esto recibe como parametro una GList y dos punteros
a funcion, donde la segunda funcion va a ser la encargada de copiar los nodos de la GList original y la primera funcion sera el predicado
bajo el que filtraremos la lista. Finalmente, devuelve una nueva GList compuesta por los nodos copiados que verifiquen el predicado.
*/
GList filter (GList, Predicado, Copia);

/**
Devuelve 1 si el dato esta en la GList, 0 en caso contrario.
*/
int GList_buscar(GList, Igualdad, void*);

#endif
