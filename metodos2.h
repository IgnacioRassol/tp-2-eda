#ifndef _metodos2_h
#define _metodos2_h
/**
wtoupper: wchar_t -> wchar_t
Recibe como parametros una letra y la devuelve en mayuscula.
*/
wchar_t wtoupper (wchar_t letra);

/**
wtolower: wchar_t -> wchar_t
Recibe como parametros una letra y la devuelve en minuscula.
*/
wchar_t wtolower (wchar_t letra);

/**
a_minuscula: wchar_t* -> wchar_t*
Recibe como parametros una cadena y devuelve la misma cadena
con todas las letras en minuscula.
*/
wchar_t* a_minuscula (wchar_t palabra[]);

/**
separar_palabra: wchar_t*, int, int, wchar_t*, wchar_t*
Recibe como parametro una palabra, su largo, la posicion en la cual queremos separar la
palabra y 2 arrays para guardar el resultado. Para entender esta funcion es importante
tener en cuenta que dentro del programa se llama siempre dentro de un for, donde la
posicion itera entre 1 y el largo de la palabra - 1. En cada iteracion, copia las letras
entre la posicion 0 de nuestra palabra y la posicion reciba como parametro en el primer
array de resultado, y el resto de la palabra en el segundo.
*/
void separar_palabra (wchar_t palabra[], int largoPalabra, int posicion,
                      wchar_t* palabra1);

/**
intercambio: wchar_t*, int, int, wchar_t*
Recibe como parametro una palabra, su largo, la posicion a intercambiar y una copia de la palabra.
Al igual que la funcion "separar_palabra" se llama dentro un for con las mismas caracteristicas.
Intercambia la letra numero "posicion" con la numero "posicion -1" en la copia de la palabra.
A partir de la posicion 2, acomda el cambio realizado en la iteracion anterior previamente a intercambiar
*/
void intercambio (wchar_t palabra[], int largoPalabra, int posicion, wchar_t* output);

/**
eliminacion: wchar_t*, int, int, wchar_t*
Recibe como parametro una palabra, su largo, la posicion a eliminar y un array para guardar el resultado.
Copia las letras de la palabra al array resultado una a una excepto la letra de la posicion indicada.
*/
void eliminacion(wchar_t palabra[], int largoPalabra, int indice, wchar_t* nuevaPalabra);

/**
agregar_entre_letra: wchar_t*, wchar_t*, int, int, wchar_t
Recibe como parametro una palabra, un array para guardar el resultado, el largo del resultado, el indice
donde se va a colocar la nueva letra y la letra a colocar. Al igual que la funcion "separar_palabra" se
llama dentro un for con las mismas caracteristicas. Ademas, por cada iteracion del for, itera sobre nuestro
alfabeto llamando a la funcion tantas veces como letras haya en el alfabeto por cada indice posible. Para
la primer letra, copia toda la palabra en el resultado dejando vacio la posicion correspondiente al indice
a cambiar. Finalmente, agrega la letra en ese indice.
*/
void agregar_entre_letras (wchar_t palabra[], wchar_t* nuevaPalabra, int largoNuevaPalabra, int indice, wchar_t letra);

/**
reemplazo: wchar_t*, int, wchar_t* int, wchar_t
Recibe como parametro una palabra, su largo, una copia de la palabra, el indice donde se va a colocar
la nueva letra y la letra a colocar. Al igual que la funcion "separar_palabra" se llama dentro un for con las
mismas caracteristicas. Ademas, por cada iteracion del for, itera sobre nuestro alfabeto llamando a la funcion
tantas veces como letras haya en el alfabeto por cada indice posible. Para le primer letra y los indices
mayores a 0, acomda el cambio realizado en la iteracion anterior previamente a reemplazar.
*/
void reemplazo (wchar_t palabra[], int largoPalabra, wchar_t* nuevaPalabra, int indice, wchar_t letra);

#endif
