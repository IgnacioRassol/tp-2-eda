#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <locale.h>

#include "hash.h"

TablaHash* tablahash_crear(int capacidad, FuncionHash hash, Igualdad ig, Destruir borrardato) {

    TablaHash* tabla = malloc(sizeof(TablaHash));
    tabla->hash = hash;
    tabla->igualdad = ig;
    tabla->destruir = borrardato;

    tabla->capacidad = capacidad;
    tabla->tabla = malloc(sizeof(GList) * capacidad);
    tabla->numElems = 0;

  // Inicializamos las casillas con listas vacias.
    for (int i = 0; i < capacidad; ++i) {
        tabla->tabla[i] = NULL;
    }

    return tabla;
}

void tablahash_insertar(TablaHash* tabla, void* dato) {
    unsigned i = tabla->hash(dato);
    int dato_repetido = 0;
    i = i % tabla->capacidad;

    if(tabla->tabla[i] != NULL){
        GList lista = tabla->tabla[i];
        dato_repetido = GList_buscar(lista, tabla->igualdad, dato);

        if (!dato_repetido) {
            GList_agregar_final(lista, dato);
            tabla->numElems++;
        }
    } else {
        tabla->tabla[i] = GList_crear();
        GList_agregar_final(tabla->tabla[i], dato);
    }
}

int tablahash_buscar(TablaHash* tabla, void* dato) {
    // Calculamos la posición de la clave dada, de acuerdo a la función hash.
    unsigned i = 0;
    i = tabla->hash(dato);
    i = i % tabla->capacidad;

    if (tabla->tabla[i] != NULL){
        if(GList_buscar(tabla->tabla[i], tabla->igualdad, dato))
            return 1;
    }

    return 0;
}


void tablahash_destruir (TablaHash* tabla) {
    for (int i = 0; i < tabla->capacidad; i++){
        if(tabla->tabla[i] != NULL)
            GList_destruir(tabla->tabla[i], tabla->destruir);
    }

    free(tabla->tabla);
    free(tabla);
}

void tablahash_diagnostico(TablaHash* tabla, Imprimir imp) {
    FILE* archivo;
    archivo = fopen("tabladistribucion","w");

    for (int i = 0; i < tabla->numElems; i++){

        fprintf(archivo, "%d \n", tabla->tabla[i]->cantidad_elementos);
    }

    fclose(archivo);
}
