#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <locale.h>
#include <assert.h>

#include "metodos2.h"

wchar_t wtoupper (wchar_t letra){
    if ((letra >= 97 && letra <= 122) || letra >= 225){
        return (letra - 32);
    } else {
        return letra;
    }
}

wchar_t wtolower (wchar_t letra){
    if ((letra >= 65 && letra <= 90) || (letra <= 220 && letra >= 193)){
        return (letra + 32);
    } else {
        return letra;
    }
}

void separar_palabra (wchar_t palabra[], int largoPalabra, int posicion, wchar_t* palabra1){
    palabra1[posicion-1] = palabra[posicion-1];
    palabra1[posicion] = L' ';

    for (int i = posicion+1; i < largoPalabra; i++) {
        palabra1[i] = palabra[i-1];
    }
}

void intercambio (wchar_t palabra[], int largoPalabra, int posicion, wchar_t* output){

    if (posicion > 0){
        if (posicion > 1){
            output[posicion-2] = palabra[posicion-2];
        }

        output[posicion] = palabra[posicion-1];
        output[posicion-1] = palabra[posicion];
    }
}

void eliminacion(wchar_t palabra[], int largoPalabra, int indice, wchar_t* nuevaPalabra){
    int contadorPalabra, indiceNuevaPalabra = 0;

    for (contadorPalabra = 0; palabra[contadorPalabra]; contadorPalabra++){
        if (contadorPalabra != indice){
            nuevaPalabra[indiceNuevaPalabra] = palabra[contadorPalabra];
            indiceNuevaPalabra++;
        }
    }

    nuevaPalabra[indiceNuevaPalabra] = '\0';
}

void agregar_entre_letras (wchar_t palabra[], wchar_t* nuevaPalabra, int largoNuevaPalabra, int indice, wchar_t letra){  //anda

    int contador = 0;

    if (letra == L'a'){
        for (int i = 0; i < largoNuevaPalabra; i++){  //Copia las letras excepto en el indice a cambiar
            if (i != indice){
                nuevaPalabra[i] = palabra[contador];
                contador++;
            }
        }
    }

    nuevaPalabra[indice] = letra;
    nuevaPalabra[largoNuevaPalabra] = 0;

}

void reemplazo (wchar_t palabra[], int largoPalabra, wchar_t* nuevaPalabra, int indice, wchar_t letra){  //anda

    if(letra == L'a' && indice > 0)
        nuevaPalabra[indice-1] = palabra[indice-1];

    nuevaPalabra[indice] = letra;
}
